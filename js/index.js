$(function() {
    // script ya incluido en Bootstrap
    $('[data-toggle="tooltip"]').tooltip();
    // funcion activa popover
    $('[data-toggle="popover"]').popover()
        // funcion carousel
    $('.carousel').carousel({
        interval: 2500
    });

    $('#contactoModal').on('show.bs.modal', function(e) {
        console.log('Inicia el modal se esta mostrando');
        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-info');
        $('#contactoBtn').prop('disabled', true);
    })
    $('#contactoModal').on('shown.bs.modal', function(e) {
        console.log('y ahora muestra su contenido');
    })
    $('#contactoModal').on('hide.bs.modal', function(e) {
        console.log('empieza a ocultarse el modal');
    })
    $('#contactoModal').on('hidden.bs.modal', function(e) {
        console.log('finalmente el modal se oculto');
        $('#contactoBtn').removeClass('btn-info');
        $('#contactoBtn').addClass('btn-outline-success');
        $('#contactoBtn').prop('disabled', false);
    })
})