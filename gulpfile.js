'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var del = require('del');
var imagemin = require('gulp-imagemin');
var uglify = require('gulp-uglify');
var rev = require('gulp-rev');
var cleanCss = require('gulp-clean-css');
var flatmap = require('gulp-flatmap');
var htmlmin = require('gulp-htmlmin');
var usemin = require('gulp-usemin');

sass.compiler = require('node-sass');

gulp.task('sass', () => {
    return gulp.src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
});

gulp.task('sass:watch', () => {
    gulp.watch('./css/*.scss', gulp.series(gulp.parallel('sass')));
});

gulp.task('browser-sync', () => {
    var files = ['./*.html', './css/*.css', './images/*.{png, jpg, gif, svg}', './js/*.js'];
    browserSync.init(files, {
        server: {
            baseDir: './'
        }
    });
});

gulp.task('default',
    gulp.series(gulp.parallel('browser-sync', 'sass:watch'))
);

gulp.task('clean', () => {
    return del(['dist']);
});

gulp.task('copyfonts', () => {
    return gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf, woff, eof, svg, eot, otf}*')
        .pipe(gulp.dest('./dist/fonts'));
});


gulp.task('imagemin', () => {
    return gulp.src('images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images'));
});

gulp.task('usemin', () => {
    return gulp.src('./*.html')
        .pipe(flatmap((stream, file) => {
            return stream
                .pipe(usemin({
                    css: [rev],
                    html: [() => {
                        return htmlmin({ collapseWhitespace: true });
                    }],
                    js: [uglify, rev],
                    inlinejs: [uglify],
                    inlinecss: [cleanCss, 'concat']
                }));
        }))
        .pipe(gulp.dest('dist/'));
});


gulp.task('build',
    gulp.series(gulp.parallel('clean', 'copyfonts', 'imagemin', 'usemin'))
);